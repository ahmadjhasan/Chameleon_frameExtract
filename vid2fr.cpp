#include "FlyCapture2.h"
#include <iostream> // for standard I/O
#include <string>   // for strings
#include <sstream>  // string to number conversion
#include <fstream>
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc/imgproc.hpp>  // Gaussian Blur
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O


using namespace FlyCapture2;
using namespace cv;

int main()
{
	Error error;
	Camera camera;
	CameraInfo camInfo;
	

	// Connect the camera
    error = camera.Connect( 0 );
    if ( error != PGRERROR_OK )
    {
        std::cout << "Failed to connect to camera" << std::endl;     
        return false;
    }
    
    // Get the camera info and print it out
    error = camera.GetCameraInfo( &camInfo );
    if ( error != PGRERROR_OK )
    {
        std::cout << "Failed to get camera info from camera" << std::endl;     
        return false;
    }
    std::cout << camInfo.vendorName << " "
    		  << camInfo.modelName << " " 
    		  << camInfo.serialNumber << std::endl;
	
	error = camera.StartCapture();
    if ( error == PGRERROR_ISOCH_BANDWIDTH_EXCEEDED )
    {
        std::cout << "Bandwidth exceeded" << std::endl;     
        return false;
    }
    else if ( error != PGRERROR_OK )
    {
        std::cout << "Failed to start image capture" << std::endl;     
        return false;
    } 
    
    
     	
	
	// capture loop
	char key = 0;
    while(key != 'q')
	{
		// Get the image
		Image rawImage;
		Error error = camera.RetrieveBuffer( &rawImage );
		if ( error != PGRERROR_OK )
		{
			std::cout << "capture error" << std::endl;
			continue;
		}
		
		// convert to rgb
	    Image rgbImage;
        rawImage.Convert( FlyCapture2::PIXEL_FORMAT_BGR, &rgbImage );
       
		// convert to OpenCV Mat
		unsigned int rowBytes = (double)rgbImage.GetReceivedDataSize()/(double)rgbImage.GetRows();       
		cv::Mat image = cv::Mat(rgbImage.GetRows(), rgbImage.GetCols(), CV_8UC3, rgbImage.GetData(),rowBytes);
		cv::Mat image1 = cv::Mat(rgbImage.GetRows(), rgbImage.GetCols(), CV_8UC3, rgbImage.GetData(),rowBytes);
		
		cv::imshow("image", image);
		cv::imshow("continue_image",image1);
		//key = cv::waitKey(30);    
		
	
		char ch =  cvWaitKey(25);  // Wait for 25 ms for user to hit any key
		if(ch==27) break;  // If Escape Key was hit just exit the loop
       
       
       
       
		if(ch=='s'){
		int i;
		i+=1;
		for(i;;i++)
		{
		std::stringstream name;
		name << "/home/ashraf/Aumi_opencv_tutorial/chamelion_vid/Image" << i << ".jpg";
		imwrite(name.str(),image);
      
		break;
       
		}
		}
	}
	
	
	error = camera.StopCapture();
    if ( error != PGRERROR_OK )
    {
        // This may fail when the camera was removed, so don't show 
        // an error message
    }  
	
	camera.Disconnect();
	
	return 0;
}